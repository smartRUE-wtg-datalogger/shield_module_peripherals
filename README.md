# Pin assignment 

### ADC 1
* PA0 -> IH1DC_DIFF
* PA3 -> IPC
* PA4 -> IH2DC_DIFF
* PA5 -> IPA
* PA6 -> IPB

### ADC2 
* PBO -> VHDC
* PB1 -> NC
* PC0 -> VPC
* PC2 -> VPB
* PC3 -> VPA

### ADC3
* PF3 -> NC
* PF4 -> NC
* PF5 -> NC
* PF6 -> NC
* PF7 -> NC
* PF8 -> NC
* PF9 -> NC
* PF10 -> NC