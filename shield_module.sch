EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Board_MCU144
LIBS:shield_module-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Micro_SD_Card CON1
U 1 1 59108B16
P 3020 1500
F 0 "CON1" H 2370 2100 50  0000 C CNN
F 1 "Micro_SD_Card" H 3670 2100 50  0000 R CNN
F 2 "footprint:AMP-114-00841-68" H 4170 1800 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/11400841-283410.pdf" H 3020 1500 50  0001 C CNN
F 4 "http://gr.mouser.com/ProductDetail/Amphenol-Commercial-Products/1140084168/?qs=sGAEpiMZZMuJakaoiLiBph8FsDENMA%2fPYqiScc5bEa4%3d" H 3020 1500 60  0001 C CNN "mouser"
	1    3020 1500
	1    0    0    -1  
$EndComp
Text GLabel 1280 1900 0    39   Input ~ 0
SDIO_DAT1
Text GLabel 1280 1800 0    39   Input ~ 0
SDIO_DAT0
Text GLabel 1270 1400 0    39   Input ~ 0
SDIO_CMD
Text GLabel 1260 1600 0    39   Input ~ 0
SDIO_CLK
Text GLabel 1280 1300 0    39   Input ~ 0
SDIO_DAT3
$Comp
L R_Small R1
U 1 1 5910992A
P 1510 930
F 0 "R1" H 1540 950 50  0000 L CNN
F 1 "10K" H 1540 890 50  0001 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 1510 930 50  0001 C CNN
F 3 "" H 1510 930 50  0000 C CNN
	1    1510 930 
	1    0    0    -1  
$EndComp
$Comp
L R_Small R2
U 1 1 591099FC
P 1660 930
F 0 "R2" H 1690 950 50  0000 L CNN
F 1 "47K" H 1690 890 50  0001 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 1660 930 50  0001 C CNN
F 3 "" H 1660 930 50  0000 C CNN
	1    1660 930 
	1    0    0    -1  
$EndComp
$Comp
L R_Small R3
U 1 1 59109A4C
P 1810 930
F 0 "R3" H 1840 950 50  0000 L CNN
F 1 "47K" H 1840 890 50  0001 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 1810 930 50  0001 C CNN
F 3 "" H 1810 930 50  0000 C CNN
	1    1810 930 
	1    0    0    -1  
$EndComp
$Comp
L R_Small R5
U 1 1 59109A7B
P 1950 930
F 0 "R5" H 1980 950 50  0000 L CNN
F 1 "47K" H 1980 890 50  0001 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 1950 930 50  0001 C CNN
F 3 "" H 1950 930 50  0000 C CNN
	1    1950 930 
	1    0    0    -1  
$EndComp
$Comp
L R_Small R6
U 1 1 59109ADD
P 2070 930
F 0 "R6" H 2100 950 50  0000 L CNN
F 1 "47K" H 2100 890 50  0001 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 2070 930 50  0001 C CNN
F 3 "" H 2070 930 50  0000 C CNN
	1    2070 930 
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 59109DFF
P 1650 2180
F 0 "C1" H 1660 2250 50  0000 L CNN
F 1 "100uF" H 1660 2100 50  0001 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1650 2180 50  0001 C CNN
F 3 "" H 1650 2180 50  0000 C CNN
	1    1650 2180
	0    -1   -1   0   
$EndComp
$Comp
L LED D1
U 1 1 5910AADF
P 1520 2460
F 0 "D1" H 1520 2560 50  0000 C CNN
F 1 "LED" H 1520 2360 50  0000 C CNN
F 2 "LEDs:LED_0603" H 1520 2460 50  0001 C CNN
F 3 "" H 1520 2460 50  0000 C CNN
	1    1520 2460
	1    0    0    -1  
$EndComp
$Comp
L R_Small R4
U 1 1 5910AC3B
P 1830 2460
F 0 "R4" H 1860 2480 50  0000 L CNN
F 1 "330 ohm" H 1860 2420 50  0001 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 1830 2460 50  0001 C CNN
F 3 "" H 1830 2460 50  0000 C CNN
	1    1830 2460
	0    -1   -1   0   
$EndComp
$Comp
L RJ45 J1
U 1 1 5910BF48
P 1260 4060
F 0 "J1" H 1460 4560 50  0000 C CNN
F 1 "RJ45" H 1110 4560 50  0000 C CNN
F 2 "footprint:rj45-noshield" H 1260 4060 50  0001 C CNN
F 3 "" H 1260 4060 50  0000 C CNN
	1    1260 4060
	1    0    0    -1  
$EndComp
$Comp
L RJ45 J3
U 1 1 5910C05C
P 2500 4070
F 0 "J3" H 2700 4570 50  0000 C CNN
F 1 "RJ45" H 2350 4570 50  0000 C CNN
F 2 "footprint:rj45-noshield" H 2500 4070 50  0001 C CNN
F 3 "" H 2500 4070 50  0000 C CNN
	1    2500 4070
	1    0    0    -1  
$EndComp
$Comp
L RJ45 J4
U 1 1 5910C10A
P 3710 4070
F 0 "J4" H 3910 4570 50  0000 C CNN
F 1 "RJ45" H 3560 4570 50  0000 C CNN
F 2 "footprint:rj45-noshield" H 3710 4070 50  0001 C CNN
F 3 "" H 3710 4070 50  0000 C CNN
	1    3710 4070
	1    0    0    -1  
$EndComp
Text GLabel 7790 1190 2    39   Input ~ 0
SDIO_DAT3
Text GLabel 7800 1290 2    39   Input ~ 0
SDIO_CMD
Text GLabel 6270 1290 0    39   Input ~ 0
SDIO_CLK
Text GLabel 10110 1190 2    39   Input ~ 0
SDIO_DAT0
Text GLabel 8610 1190 0    39   Input ~ 0
SDIO_DAT1
Wire Wire Line
	1280 1200 2120 1200
Wire Wire Line
	1280 1300 2120 1300
Wire Wire Line
	1270 1400 2120 1400
Wire Wire Line
	1260 1600 2120 1600
Wire Wire Line
	1280 1800 2120 1800
Wire Wire Line
	1280 1900 2120 1900
Wire Wire Line
	1510 1030 1510 1400
Connection ~ 1510 1400
Wire Wire Line
	1660 1030 1660 1900
Connection ~ 1660 1900
Wire Wire Line
	1810 1030 1810 1800
Connection ~ 1810 1800
Wire Wire Line
	1950 1030 1950 1300
Connection ~ 1950 1300
Wire Wire Line
	2070 1030 2070 1200
Connection ~ 2070 1200
Wire Wire Line
	1490 1500 2120 1500
Wire Wire Line
	2120 1700 1500 1700
Wire Wire Line
	1350 760  2070 760 
Wire Wire Line
	2070 760  2070 830 
Wire Wire Line
	1950 830  1950 760 
Connection ~ 1950 760 
Wire Wire Line
	1810 830  1810 760 
Connection ~ 1810 760 
Wire Wire Line
	1660 830  1660 760 
Connection ~ 1660 760 
Wire Wire Line
	1510 830  1510 760 
Connection ~ 1510 760 
Wire Wire Line
	1410 2180 1550 2180
Wire Wire Line
	1290 2460 1370 2460
Wire Wire Line
	2000 2460 1930 2460
Wire Wire Line
	1730 2460 1670 2460
Wire Wire Line
	9970 1190 10110 1190
Wire Wire Line
	8720 1190 8610 1190
Wire Wire Line
	6420 1190 6300 1190
Wire Wire Line
	7670 1190 7790 1190
Wire Wire Line
	6420 1290 6270 1290
Wire Wire Line
	7670 1290 7800 1290
Wire Wire Line
	1510 4630 1510 4510
Wire Wire Line
	1610 4510 1610 4590
Wire Wire Line
	1410 4590 1410 4510
Wire Wire Line
	1310 4510 1310 4630
Wire Wire Line
	1210 4510 1210 4590
Wire Wire Line
	1110 4510 1110 4630
Wire Wire Line
	910  4510 910  4630
Wire Wire Line
	910  4540 1010 4540
Wire Wire Line
	1010 4540 1010 4510
Connection ~ 910  4540
Text Label 1610 4590 3    39   ~ 0
IPA
Text Label 1510 4630 3    39   ~ 0
IPN
Text Label 1310 4630 3    39   ~ 0
IPN
Text Label 1110 4630 3    39   ~ 0
IPN
Text Label 1410 4590 3    39   ~ 0
IPB
Text Label 1210 4590 3    39   ~ 0
IPC
Text Label 2850 4590 3    39   ~ 0
VPA
Text Label 2750 4630 3    39   ~ 0
VPN
Wire Wire Line
	2750 4630 2750 4520
Wire Wire Line
	2850 4520 2850 4590
Text Label 2650 4590 3    39   ~ 0
VPB
Text Label 2450 4590 3    39   ~ 0
VPC
Text Label 2350 4640 3    39   ~ 0
VPN
Text Label 2550 4640 3    39   ~ 0
VPN
Wire Wire Line
	2550 4640 2550 4520
Wire Wire Line
	2650 4590 2650 4520
Wire Wire Line
	2450 4520 2450 4590
Wire Wire Line
	2350 4520 2350 4640
Wire Wire Line
	2150 4520 2150 4610
Wire Wire Line
	2250 4520 2250 4550
Wire Wire Line
	2250 4550 2150 4550
Connection ~ 2150 4550
Text Label 4060 4590 3    39   ~ 0
VHDC_DIFF
Text Label 3960 4630 3    39   ~ 0
VLDC_DIFF
Wire Wire Line
	3960 4520 3960 4630
Wire Wire Line
	4060 4520 4060 4590
Wire Wire Line
	3760 4520 3760 4650
Wire Wire Line
	3760 4650 3860 4650
Wire Wire Line
	3860 4650 3860 4520
Connection ~ 3810 4650
Text Label 3360 4630 3    39   ~ 0
IL1DC_DIFF
Text Label 3460 4590 3    39   ~ 0
IH1DC_DIFF
Wire Wire Line
	3360 4520 3360 4630
Wire Wire Line
	3460 4590 3460 4520
Text Label 3660 4590 3    39   ~ 0
IH2DC_DIFF
Wire Wire Line
	3660 4590 3660 4520
Wire Wire Line
	3560 4630 3560 4520
Text Label 3560 4630 3    39   ~ 0
IL2DC_DIFF
NoConn ~ 1810 3710
NoConn ~ 3050 3720
NoConn ~ 4260 3720
Text Label 1350 760  2    39   ~ 0
+3.3V
Text Label 7790 1890 0    39   ~ 0
+3.3V
Wire Wire Line
	7670 1890 7790 1890
Text Label 7820 4990 0    39   ~ 0
GND
Text Label 7800 2090 0    39   ~ 0
GND
Wire Wire Line
	7670 4990 7820 4990
Text Label 7820 4290 0    39   ~ 0
GND
Wire Wire Line
	7820 4290 7670 4290
Text Label 6290 4990 2    39   ~ 0
GND
Wire Wire Line
	6290 4990 6420 4990
Text Label 8610 4990 2    39   ~ 0
GND
Wire Wire Line
	8610 4990 8720 4990
Text Label 10120 4990 0    39   ~ 0
GND
Wire Wire Line
	10120 4990 9970 4990
Text Label 8610 4490 2    39   ~ 0
GND
Wire Wire Line
	8610 4490 8720 4490
Text Label 8580 3290 2    39   ~ 0
GND
Wire Wire Line
	8580 3290 8720 3290
Text Label 8580 1590 2    39   ~ 0
GND
Wire Wire Line
	8720 1590 8580 1590
Wire Wire Line
	3810 4650 3810 4720
Text Label 6280 3790 2    39   ~ 0
GND
Wire Wire Line
	6420 3790 6280 3790
Text Label 6280 2090 2    39   ~ 0
GND
Wire Wire Line
	6280 2090 6420 2090
Text Label 7800 1490 0    39   ~ 0
GND
Wire Wire Line
	7800 1490 7670 1490
$Comp
L Nucleo_Board_144 Board1
U 1 1 59108E3A
P 8220 3090
F 0 "Board1" H 8220 5240 60  0000 L BNN
F 1 "Nucleo_Board_144" H 7920 940 60  0000 L BNN
F 2 "footprint:Nucleo_Board144" H 6620 5090 60  0001 C CNN
F 3 "Morpho connectors" H 6620 5090 60  0000 C CNN
	1    8220 3090
	1    0    0    -1  
$EndComp
Text Label 7800 2190 0    39   ~ 0
GND
Wire Wire Line
	7800 2190 7670 2190
Wire Wire Line
	7670 2090 7800 2090
Text Label 10140 2090 0    39   ~ 0
GND
Wire Wire Line
	10140 2090 9970 2090
Text Label 10160 3990 0    39   ~ 0
GND
Wire Wire Line
	10160 3990 9970 3990
NoConn ~ 6420 1390
NoConn ~ 6420 1490
NoConn ~ 6420 1590
NoConn ~ 6420 1690
NoConn ~ 6420 1790
NoConn ~ 6420 1890
NoConn ~ 6420 1990
NoConn ~ 6420 2190
NoConn ~ 6420 2290
NoConn ~ 6420 2390
NoConn ~ 6420 2490
NoConn ~ 6420 2590
NoConn ~ 6420 2690
NoConn ~ 6420 2790
NoConn ~ 6420 3590
NoConn ~ 6420 3690
NoConn ~ 6420 3890
NoConn ~ 6420 3990
NoConn ~ 6420 4290
NoConn ~ 6420 4390
NoConn ~ 6420 4490
NoConn ~ 6420 4590
NoConn ~ 6420 4690
NoConn ~ 7670 4790
NoConn ~ 7670 4690
NoConn ~ 7670 4590
NoConn ~ 7670 4490
NoConn ~ 7670 4390
NoConn ~ 7670 4190
NoConn ~ 7670 4090
NoConn ~ 7670 3990
NoConn ~ 7670 3890
NoConn ~ 7670 3790
NoConn ~ 7670 3690
NoConn ~ 7670 3590
NoConn ~ 7670 3490
NoConn ~ 7670 3390
NoConn ~ 7670 2890
NoConn ~ 7670 2590
NoConn ~ 7670 2390
NoConn ~ 7670 2290
NoConn ~ 7670 1990
NoConn ~ 7670 1790
NoConn ~ 7670 1690
NoConn ~ 7670 1590
NoConn ~ 7670 1390
NoConn ~ 8720 1290
NoConn ~ 8720 1390
NoConn ~ 8720 1490
NoConn ~ 8720 1890
NoConn ~ 8720 2090
NoConn ~ 8720 2190
NoConn ~ 8720 2290
NoConn ~ 8720 2390
NoConn ~ 8720 2490
NoConn ~ 8720 2590
NoConn ~ 8720 2690
NoConn ~ 8720 2790
NoConn ~ 8720 2890
NoConn ~ 8720 3390
NoConn ~ 8720 3690
NoConn ~ 8720 3790
NoConn ~ 8720 3890
NoConn ~ 8720 3990
NoConn ~ 8720 4090
NoConn ~ 8720 4190
NoConn ~ 8720 4290
NoConn ~ 8720 4390
NoConn ~ 8720 4590
NoConn ~ 8720 4690
NoConn ~ 8720 4790
NoConn ~ 9970 4790
NoConn ~ 9970 4690
NoConn ~ 9970 4590
NoConn ~ 9970 4490
NoConn ~ 9970 4390
NoConn ~ 9970 4290
NoConn ~ 9970 4190
NoConn ~ 9970 4090
NoConn ~ 9970 3890
NoConn ~ 9970 3790
NoConn ~ 9970 3690
NoConn ~ 9970 3590
NoConn ~ 9970 3490
NoConn ~ 9970 3390
NoConn ~ 9970 3290
NoConn ~ 9970 2990
NoConn ~ 9970 2890
NoConn ~ 9970 2790
NoConn ~ 9970 2590
NoConn ~ 9970 2490
NoConn ~ 9970 2390
NoConn ~ 9970 2190
NoConn ~ 9970 1990
NoConn ~ 9970 1790
NoConn ~ 9970 1690
NoConn ~ 9970 1490
NoConn ~ 9970 1390
NoConn ~ 9970 1290
Text Label 10140 2690 0    39   ~ 0
GNDA
Wire Wire Line
	10140 2690 9970 2690
Text Label 910  4630 3    39   ~ 0
GNDA
Text Label 2150 4610 3    39   ~ 0
GNDA
Text Label 3810 4720 3    39   ~ 0
GNDA
Text Label 1890 2180 0    39   ~ 0
+3.3V
Wire Wire Line
	1890 2180 1750 2180
Text Label 2000 2460 0    39   ~ 0
+3.3V
Text Label 1490 1500 2    39   ~ 0
+3.3V
Text Label 1410 2180 2    39   ~ 0
GND
Text Label 1290 2460 2    39   ~ 0
GND
Text Label 1500 1700 2    39   ~ 0
GND
Text Label 1280 1200 2    39   ~ 0
SDIO_DAT2
Text Label 6300 1190 2    39   ~ 0
SDIO_DAT2
Text Label 8580 1690 2    39   ~ 0
IPA
Text Label 6430 5570 2    39   ~ 0
IPN
Text Label 8590 1790 2    39   ~ 0
IPB
Text Label 8580 2990 2    39   ~ 0
IPC
Wire Wire Line
	8580 1690 8720 1690
Wire Wire Line
	8590 1790 8720 1790
Text Label 6640 5570 0    39   ~ 0
GNDA
Text Label 6440 5690 2    39   ~ 0
VPN
Wire Wire Line
	6430 5570 6640 5570
Text Label 6640 5690 0    39   ~ 0
GNDA
Wire Wire Line
	6440 5690 6640 5690
Text Label 6270 2990 2    39   ~ 0
VPA
Text Label 6270 2890 2    39   ~ 0
VPB
Text Label 7810 2990 0    39   ~ 0
VPC
Wire Wire Line
	8580 2990 8720 2990
Wire Wire Line
	7820 2790 7670 2790
Wire Wire Line
	7810 2990 7670 2990
Wire Wire Line
	6270 2890 6420 2890
Text Label 6420 6060 2    39   ~ 0
VLDC_DIFF
Text Label 7820 2790 0    39   ~ 0
VHDC_DIFF
Wire Wire Line
	7670 2490 7820 2490
Wire Wire Line
	7670 2690 7820 2690
Text Label 6420 6170 2    39   ~ 0
IL1DC_DIFF
Text Label 6420 6280 2    39   ~ 0
IL2DC_DIFF
Text Label 6640 6060 0    39   ~ 0
GNDA
Text Label 6640 6170 0    39   ~ 0
GNDA
Text Label 6640 6280 0    39   ~ 0
GNDA
Wire Wire Line
	6420 6060 6640 6060
Wire Wire Line
	6420 6170 6640 6170
Wire Wire Line
	6420 6280 6640 6280
Text Label 7820 2490 0    39   ~ 0
IH1DC_DIFF
Text Label 7820 2690 0    39   ~ 0
IH2DC_DIFF
Wire Wire Line
	6270 2990 6420 2990
Text Label 3190 5650 0    60   ~ 0
CAN1_RX
Text Label 3280 5750 0    60   ~ 0
CAN1_TX
Text Label 3190 5950 0    39   ~ 0
CAN2_RX
Text Label 3280 6050 0    39   ~ 0
CAN2_TX
Wire Wire Line
	3190 5650 2990 5650
Wire Wire Line
	3190 5950 2990 5950
Wire Wire Line
	3280 6050 2990 6050
Text Label 3120 6250 0    39   ~ 0
UA1_RX
Text Label 3120 6350 0    39   ~ 0
UA1_TX
Text Label 3120 6450 0    39   ~ 0
UA1_RTS
Text Label 3120 6550 0    39   ~ 0
UA1_CTS
Text Label 3120 6740 0    39   ~ 0
UA2_RX
Text Label 3120 6840 0    39   ~ 0
UA2_TX
Text Label 3120 6940 0    39   ~ 0
UA2_RTS
Text Label 3120 7040 0    39   ~ 0
UA2_CTS
Wire Wire Line
	2990 6250 3120 6250
Wire Wire Line
	2990 6350 3120 6350
Wire Wire Line
	2990 6450 3120 6450
Wire Wire Line
	2990 6550 3120 6550
Wire Wire Line
	2990 6740 3120 6740
Wire Wire Line
	2990 6840 3120 6840
Wire Wire Line
	2990 6940 3120 6940
Wire Wire Line
	2990 7040 3120 7040
Text Label 6280 4190 2    39   ~ 0
CAN1_RX
Text Label 6280 4090 2    39   ~ 0
CAN1_TX
Text Label 10140 1890 0    39   ~ 0
CAN2_RX
Text Label 8590 1990 2    39   ~ 0
CAN2_TX
Wire Wire Line
	9970 1890 10140 1890
Wire Wire Line
	8590 1990 8720 1990
Wire Wire Line
	6280 4190 6420 4190
Wire Wire Line
	6280 4090 6420 4090
$Comp
L CONN_01X04 P2
U 1 1 591EFCB9
P 2790 6400
F 0 "P2" H 2790 6650 50  0000 C CNN
F 1 "CONN_01X04" V 2890 6400 50  0000 C CNN
F 2 "Connectors_Molex:Molex_SPOX-5267_22-03-5045_04x2.54mm_Straight" H 2790 6400 50  0001 C CNN
F 3 "http://uk.rs-online.com/web/p/pcb-headers/4838499/" H 2790 6400 50  0001 C CNN
	1    2790 6400
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X04 P3
U 1 1 591EFD2C
P 2790 6890
F 0 "P3" H 2790 7140 50  0000 C CNN
F 1 "CONN_01X04" V 2890 6890 50  0000 C CNN
F 2 "Connectors_Molex:Molex_SPOX-5267_22-03-5045_04x2.54mm_Straight" H 2790 6890 50  0001 C CNN
F 3 "http://uk.rs-online.com/web/p/pcb-headers/4838499/" H 2790 6890 50  0001 C CNN
	1    2790 6890
	-1   0    0    1   
$EndComp
Text Label 6270 3490 2    39   ~ 0
UA1_RX
Text Label 6270 3390 2    39   ~ 0
UA1_TX
Text Label 6270 3290 2    39   ~ 0
UA1_RTS
Text Label 7820 3290 0    39   ~ 0
UA1_CTS
Wire Wire Line
	6270 3290 6420 3290
Wire Wire Line
	6270 3390 6420 3390
Wire Wire Line
	6270 3490 6420 3490
Wire Wire Line
	7820 3290 7670 3290
Text Label 6280 4790 2    39   ~ 0
UA2_TX
Text Label 8570 3590 2    39   ~ 0
UA2_RTS
Text Label 8570 3490 2    39   ~ 0
UA2_CTS
Text Label 10130 1590 0    39   ~ 0
UA2_RX
Wire Wire Line
	9970 1590 10130 1590
Wire Wire Line
	6420 4790 6280 4790
Wire Wire Line
	8570 3490 8720 3490
Wire Wire Line
	8720 3590 8570 3590
Wire Wire Line
	10120 2290 9970 2290
$Comp
L CONN_01X02 P1
U 1 1 591F1ABC
P 2790 5700
F 0 "P1" H 2790 5850 50  0000 C CNN
F 1 "CONN_01X02" V 2890 5700 50  0000 C CNN
F 2 "Connectors_Molex:Molex_SPOX-5267_22-03-5025_02x2.54mm_Straight" H 2790 5700 50  0001 C CNN
F 3 "" H 2790 5700 50  0000 C CNN
	1    2790 5700
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X02 P4
U 1 1 591F1BA2
P 2790 6000
F 0 "P4" H 2790 6150 50  0000 C CNN
F 1 "CONN_01X02" V 2890 6000 50  0000 C CNN
F 2 "Connectors_Molex:Molex_SPOX-5267_22-03-5025_02x2.54mm_Straight" H 2790 6000 50  0001 C CNN
F 3 "" H 2790 6000 50  0000 C CNN
	1    2790 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	2990 5750 3280 5750
Text Label 3820 2200 3    39   ~ 0
GND
Wire Wire Line
	3820 2100 3820 2200
$EndSCHEMATC
